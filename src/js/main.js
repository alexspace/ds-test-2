document.addEventListener("DOMContentLoaded", function (event) {

    // Micromodal

    MicroModal.init();
    const search = document.querySelector('.i-search');
    search.addEventListener('click', function () {
        const search = document.querySelector('.search-field');
        search.focus();
    });

});
