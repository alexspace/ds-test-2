document.addEventListener("DOMContentLoaded", function (event) {

    // Featured product slider

    const $slider = jQuery('.featured-slider-slick');

    if ( $slider.length > 0 ) {
        $slider.slick({
            centerMode: true,
            centerPadding: '60px',
            slidesToShow: 3,
            slidesToScroll: 1,
            initialSlide: 1,
            variableWidth: true,
            draggable: false,
            speed: 150,
            arrows: true,
            focusOnSelect: true,
            appendArrows: jQuery('.featured-slider-nav'),
            prevArrow: $('.featured-slider-nav .prev'),
            nextArrow: $('.featured-slider-nav .next'),
        });

        const index = $slider.slick('slickCurrentSlide');
        jQuery('.slide-indexes').append('<li><a href="#">' + '0' + (index) + '</a></li><li><a href="#">' + '0' + (index + 1) + '</a></li><li><a href="#">' + '0' + (index + 2) + '</a></li>');

        jQuery('.slide-indexes a').on('click', function (e) {
            e.preventDefault();
            let index = e.target.innerHTML;
            $slider.slick('slickGoTo', index - 1);
        });

        jQuery('.slick-slide a').on('click', function (e) {
            e.preventDefault();
        });

        $slider.on('beforeChange', function (e) {
            $slider.find('.slide-link').fadeOut(50);
        });

        $slider.on('afterChange', function (e, slick, currentSlide) {
            $slider.find('.slide-link').fadeIn(100);

            const slidesCount = slick.slideCount;
            jQuery('.slide-indexes li a').each(function (index, el) {
                index = index + 1;
                let newIndex;

                if (index === 1) {
                    newIndex = currentSlide;
                } else if (index === 2) {
                    newIndex = currentSlide + 1;
                } else if (index === 3) {
                    newIndex = currentSlide + 2;
                }

                if (newIndex <= 0) {
                    newIndex = slidesCount;
                } else if (newIndex > slidesCount) {
                    newIndex = 1;
                }

                el.innerHTML = '0' + newIndex;
            })
        });
    }

}); //content loaded