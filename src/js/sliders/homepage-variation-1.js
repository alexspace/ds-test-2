document.addEventListener("DOMContentLoaded", function (event) {

    // Main homepage slider

    const $mainSlider = jQuery('.main-slider-slick');

    if ( $mainSlider.length > 0 ) {
        $mainSlider.slick({
            slidesToShow: 2,
            slidesToScroll: 2,
            draggable: false,
            swipe: false,
            arrows: true,
            appendArrows: jQuery('.main-slider-nav'),
            prevArrow: $('.main-slider-nav .main-prev'),
            nextArrow: $('.main-slider-nav .main-next'),
            cssEase: 'ease-in-out',
            speed: 500,
            responsive: [
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        draggable: true
                    }
                }
            ],
        });

        $mainSlider.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
            $mainSlider.find('.slick-active .backlayer').each(function (index, el) {
                jQuery(el).addClass('bounceOutUp');

                setTimeout(function () {
                    jQuery(el).removeClass('bounceOutUp');
                }, 800);
            });

            $mainSlider.find('.slick-slide:not(.slick-active) .backlayer').each(function (index, el) {
                jQuery(el).addClass('bounceInUp');
                jQuery(el).addClass('jello');

                setTimeout(function () {
                    jQuery(el).removeClass('bounceInUp');
                    jQuery(el).removeClass('jello');
                }, 800);
            });
        });
    }

}); //content loaded